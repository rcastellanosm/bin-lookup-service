<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210505195545_add_issuer_bin_relation extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bin_range ADD issuer_id INT NOT NULL');
        $this->addSql('ALTER TABLE bin_range ADD CONSTRAINT FK_28CC26D2BB9D6FEE FOREIGN KEY (issuer_id) REFERENCES issuer (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_28CC26D2BB9D6FEE ON bin_range (issuer_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bin_range DROP FOREIGN KEY FK_28CC26D2BB9D6FEE');
        $this->addSql('DROP INDEX UNIQ_28CC26D2BB9D6FEE ON bin_range');
        $this->addSql('ALTER TABLE bin_range DROP issuer_id');
    }
}
