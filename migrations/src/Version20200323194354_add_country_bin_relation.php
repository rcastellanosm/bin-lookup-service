<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200323194354_add_country_bin_relation extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add country/bin_range relationship and index';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bin_range ADD country_id INT NOT NULL');
        $this->addSql('ALTER TABLE bin_range ADD CONSTRAINT FK_28CC26D2F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_28CC26D2F92F3E70 ON bin_range (country_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bin_range DROP FOREIGN KEY FK_28CC26D2F92F3E70');
        $this->addSql('DROP INDEX UNIQ_28CC26D2F92F3E70 ON bin_range');
        $this->addSql('ALTER TABLE bin_range DROP country_id');
    }
}
