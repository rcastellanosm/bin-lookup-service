<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200323193947_add_network_bin_range_relation extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add network/bin_range relationship and index';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bin_range ADD network_id INT NOT NULL');
        $this->addSql('ALTER TABLE bin_range ADD CONSTRAINT FK_28CC26D234128B91 FOREIGN KEY (network_id) REFERENCES network (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_28CC26D234128B91 ON bin_range (network_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bin_range DROP FOREIGN KEY FK_28CC26D234128B91');
        $this->addSql('DROP INDEX UNIQ_28CC26D234128B91 ON bin_range');
        $this->addSql('ALTER TABLE bin_range DROP network_id');
    }
}
