# Bin Lookup Service
A centralized API based service with Issuer Identification Number (IIN/BIN) database that return all information on a received BIN*

## Usage
Make a `GET` request on `http://bin_lookup_service/v1/find/{YOUR_BIN_NUMBER}` (this services ins available as inter stack premises)

## Backoffice Access
Ingress to `https://backend-{env}.bin-lookup-service.equationlabs.io.com/login` (you must be logged in on a VPN)

## Open API Contract
All API documentation can be found here -> [Open API Contract](api/src/Reference/bin-lookup-service.v1_openapi.yml)

Request:
```bash
curl -H  "accept: application/vnd.api+json" -H "country: AR" "http://bin_lookup/v1/find/45717360" 
```

Response:
```
HTTP/1.1 200 OK
Content-Type: application/vnd.api+json
```
```json
{
  "data": {
    "id": "/find/1",
    "type": "BinRange",
    "attributes": {
      "accountRangeFrom": 459354,
      "accountRangeTo": 459355,
      "brand": "black",
      "_type": "CREDIT",
      "prepaid": false,
      "cashback": false,
      "network": {
        "name": "Visa",
        "type": "Public"
      },
      "issuer": {
        "name": "Banco de Galicia y Buenos Aires S.A",
        "commonName": "Banco Galicia"
      },
      "country": {
        "name": "Argentina",
        "code": "AR"
      }
    }
  }
}
```

## Tests
We add a [Makefile](/api/Makefile) to prepare the test stage prior to run integration test from pipelines or local

```
make prepare-scenario
```
and then you can run

```
composer test:application
```

## Contributing
We use GitHub Flow in this service, you must open a pull request from master with your changes and must be covered by tests as well.

 
