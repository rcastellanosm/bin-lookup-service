#!/bin/bash

# Sets up environment for development.
# 1. Installs composer dev dependencies.
# 2. Enables xdebug.
# 3. Installs yarn dependencies and builds it.

if [ "$APP_DEBUG" = true ]; then
  su application -c "/usr/local/bin/composer install"
fi