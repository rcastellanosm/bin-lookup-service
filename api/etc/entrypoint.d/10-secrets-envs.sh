#!/bin/bash

# read secrets and assign to env vars
# shellcheck disable=SC2155
export MYSQL_URI=$(</run/secrets/mysql_uri)

# Copy env.template and substitute placeholders
envsubst < ".env.template" > ".env"