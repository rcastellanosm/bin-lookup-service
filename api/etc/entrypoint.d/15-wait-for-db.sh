#!/usr/bin/env bash

# Waits until database is fully migrated
# and unlocked by other services.

# shellcheck disable=SC2046
bin/db-locker wait -t 2 -u $(</run/secrets/mysql_uri)