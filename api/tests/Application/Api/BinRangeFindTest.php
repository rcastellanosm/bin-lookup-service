<?php

namespace App\Tests\Application\Api;

use App\Entity\BinRange;
use App\Tests\BaseApiTestCase;

class BinRangeFindTest extends BaseApiTestCase
{
    const A_VALID_BIN_RANGE = '459354';
    const INVALID_BIN_RANGE = '569001';

    /** @test */
    public function it_can_find_bin_resource_by_its_bin(): void
    {
        $this->makeApiRequest('GET', "/v1/find/" . self::A_VALID_BIN_RANGE);

        $this->assertResponseIsSuccessful();
        $this->assertMatchesResourceItemJsonSchema(BinRange::class);
        $this->assertResponseHeaderSame('content-type', 'application/vnd.api+json; charset=utf-8');
    }

    /** @test */
    public function must_thrown_an_exception_if_not_find_a_resource(): void
    {
        $this->makeApiRequest('GET', "/v1/find/" . self::INVALID_BIN_RANGE);

        $this->assertResponseStatusCodeSame(400);
    }
}
