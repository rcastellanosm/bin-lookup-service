<?php


namespace App\Tests;


use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\DataFixtures\BinRangeFixtures;
use App\DataFixtures\IssuerFixtures;
use App\DataFixtures\NetworkFixtures;
use App\DataFixtures\UserFixtures;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;

class BaseApiTestCase extends ApiTestCase
{
    const A_VALID_ACCEPT_CONTENT_TYPE = 'application/vnd.api+json';
    const A_VALID_COUNTRY = 'AR';

    public function makeApiRequest(?string $verb, ?string $resource, bool $invalid = false)
    {
        $client = static::createClient();

        return $client->request($verb, $resource, [
                'headers' => [
                    'accept' => self::A_VALID_ACCEPT_CONTENT_TYPE,
                    'country' => ($invalid) ? null : self::A_VALID_COUNTRY
                ]
            ]
        );
    }

}