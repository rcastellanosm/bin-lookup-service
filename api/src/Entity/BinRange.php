<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     itemOperations={
 *       "find_bin"={
 *          "method"="GET",
 *          "path"="/find/{id}.{_format}",
 *          "requirements"={"id"="\d+"},
 *          "dataProvider"="BinRangeItemDataProvider",
 *          "openapi_context"={
 *              "summary"="Find a BinRange Resource by its BIN",
 *              "parameters"= { {"name"="id", "type"="string", "in"="path", "required"=true, "example"="459354" }, { "name"="country", "type"="string", "in"="header", "required"=true,"example"="AR" } },
 *          }
 *       }
 *     },
 *     collectionOperations={},
 *     normalizationContext={
 *       "groups"={"bin_read"},
 *       "swagger_definition_name": "Read"
 *     }
 * )
 *
 * @ORM\Entity(repositoryClass="App\Repository\BinRangeRepository")
 */
class BinRange
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"bin_read"})
     * @ORM\Column(type="integer")
     */
    private $accountRangeFrom;

    /**
     * @Groups({"bin_read"})
     * @ORM\Column(type="integer", nullable=true)
     */
    private $accountRangeTo;

    /**
     * @Groups({"bin_read"})
     * @ORM\Column(type="string", length=255)
     */
    private $brand;

    /**
     * @Groups({"bin_read"})
     * @ORM\Column(type="string", length=50)
     */
    private $type;

    /**
     * @Groups({"bin_read"})
     * @ORM\Column(type="smallint")
     */
    private $prepaid;

    /**
     * @Groups({"bin_read"})
     * @ORM\Column(type="smallint")
     */
    private $cashback;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $modifiedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @Groups({"bin_read"})
     * @ORM\OneToOne(targetEntity="App\Entity\Network", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $network;

    /**
     * @Groups({"bin_read"})
     * @ORM\OneToOne(targetEntity=Issuer::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $issuer;

    /**
     * @Groups({"bin_read"})
     * @ORM\OneToOne(targetEntity="App\Entity\Country", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $country;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAccountRangeFrom(): ?int
    {
        return $this->accountRangeFrom;
    }

    public function setAccountRangeFrom(int $accountRangeFrom): self
    {
        $this->accountRangeFrom = $accountRangeFrom;

        return $this;
    }

    public function getAccountRangeTo(): ?int
    {
        return $this->accountRangeTo;
    }

    public function setAccountRangeTo(int $accountRangeTo): self
    {
        $this->accountRangeTo = $accountRangeTo;

        return $this;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function setBrand(string $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getPrepaid(): ?int
    {
        return $this->prepaid;
    }

    public function setPrepaid(int $prepaid): self
    {
        $this->prepaid = $prepaid;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getModifiedAt(): ?\DateTimeInterface
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt(?\DateTimeInterface $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getNetwork(): ?Network
    {
        return $this->network;
    }

    public function setNetwork(Network $network): self
    {
        $this->network = $network;

        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getIssuer(): ?Issuer
    {
        return $this->issuer;
    }

    public function setIssuer(Issuer $issuer): self
    {
        $this->issuer = $issuer;

        return $this;
    }

    public function getCashback(): ?int
    {
        return $this->cashback;
    }

    public function setCashback(int $cashback): self
    {
        $this->cashback = $cashback;

        return $this;
    }
}
