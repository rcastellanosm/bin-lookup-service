<?php

namespace App\DataProvider;


use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Entity\BinRange;
use App\Repository\BinRangeRepository;
use Symfony\Component\HttpFoundation\RequestStack;


class BinRangeItemDataProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface
{

    /**
     * @var BinRangeRepository
     */
    private $binRangeRepository;
    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct(BinRangeRepository $binRangeRepository, RequestStack $requestStack)
    {
        $this->binRangeRepository = $binRangeRepository;
        $this->requestStack = $requestStack;
    }

    /**
     * @inheritDoc
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?BinRange
    {
        // TODO: Search first on redis otherwise on DB
        $requestedBin = $this->requestStack->getCurrentRequest()->get('id');
        $country = $this->requestStack->getCurrentRequest()->headers->get('country');

        $findCriteria = [
            'bin' => $requestedBin,
            'country' => $country
        ];

        return $this->binRangeRepository->findBinRangeByCriteria($findCriteria);
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return BinRange::class === $resourceClass && 'find_bin' === $operationName;
    }
}