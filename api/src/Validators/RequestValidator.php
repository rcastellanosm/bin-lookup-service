<?php

namespace App\Validators;

use App\Exceptions\ValidationAgainstSpecFailed;
use League\OpenAPIValidation\PSR7\Exception\ValidationFailed;
use League\OpenAPIValidation\PSR7\OperationAddress;
use League\OpenAPIValidation\PSR7\ValidatorBuilder;
use Symfony\Bridge\PsrHttpMessage\Factory\PsrHttpFactory;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ServerRequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UploadedFileFactoryInterface;

use Symfony\Component\HttpFoundation\Request;


class RequestValidator
{
    /** @var PsrHttpFactory */
    private $psrHttpFactory;

    public const BIN_LOOKUP_SERVICE_OAS = './../Reference/bin-lookup-service.v1_openapi.yml';

    public function __construct(
        ServerRequestFactoryInterface $psr17Factory,
        StreamFactoryInterface $streamFactory,
        UploadedFileFactoryInterface $uploadedFileFactory,
        ResponseFactoryInterface $responseFactory
    ) {
        $this->psrHttpFactory = new PsrHttpFactory($psr17Factory, $streamFactory, $uploadedFileFactory, $responseFactory);
    }

    public function validate(Request $request): OperationAddress
    {
        $validator = (new ValidatorBuilder())->fromYamlFile(__DIR__ .'/'. self::BIN_LOOKUP_SERVICE_OAS);
        $psrRequest = $this->psrHttpFactory->createRequest($request);

        try {
            return $validator->getRequestValidator()->validate($psrRequest);
        } catch (ValidationFailed | \Exception $e) {
            throw new ValidationAgainstSpecFailed($e->getMessage());
        }
    }


}
