<?php

namespace App\Enum;


class NetworkTypeEnum extends BasicEnum
{
    public const __default = self::PUBLIC;

    public const PUBLIC = 'Public';
    public const PRIVATE = 'Private';

}