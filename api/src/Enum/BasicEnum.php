<?php

namespace App\Enum;

use Garoevans\PhpEnum\Enum;
use App\Exceptions\BasicEnumException;

abstract class BasicEnum extends Enum
{
    public static function isValidValue(string $value)
    {
        return in_array($value, (new static())->getConstList());
    }

    public static function isValidValueOrThrowException(string $value): void
    {
        if (self::isValidValue($value)) {
            return;
        }

        throw new BasicEnumException($value.' is not a valid value to enum class');
    }

    public static function isValidKey(string $const)
    {
        return key_exists($const, (new static())->getConstList());
    }

    public static function getValue(string $const)
    {
        $const = strtoupper($const);
        if (!self::isValidKey($const)) {
            throw new BasicEnumException('Enum No key found');
        }

        return (new static())->getConstList()[$const];
    }

    public static function getConstFromValue(string $value)
    {
        return (new static())->constFromValue($value);
    }

    public static function getAllConstantsAndValues()
    {
        $constantsAndValues = [];
        foreach ((new static())->getConstList() as $constant => $value) {
            $constantsAndValues[$constant] = $value;
        }

        return $constantsAndValues;
    }
}
