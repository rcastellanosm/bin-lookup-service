<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils, TranslatorInterface $translator): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('@EasyAdmin/page/login.html.twig',
            [
                'page_title' => $translator->trans('login.title', [], 'admin'),
                'username_label' => $translator->trans('login.email_placeholder', [], 'admin'),
                'username_parameter' => 'email',
                'password_parameter' => 'password',
                'last_username' => $lastUsername,
                'error' => $error,
                'translation_domain' => 'admin',
                'csrf_token_intention' => 'authenticate'
            ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        return $this->redirect('app_login');
    }

}
