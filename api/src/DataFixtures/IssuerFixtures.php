<?php

namespace App\DataFixtures;

use App\Entity\Country;
use App\Entity\Issuer;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class IssuerFixtures extends BaseFixture implements DependentFixtureInterface
{
    const ISSUER_FIXTURE_REFERENCE = 'galicia-issuer';

    private $galicia = [
        'name' => 'Banco de Galicia y Buenos Aires S.A',
        'commonName' => 'Banco Galicia'
    ];

    protected function loadData(ObjectManager $manager)
    {
        $issuer = new Issuer();
        $issuer->setName($this->galicia['name']);
        $issuer->setCommonName($this->galicia['commonName']);

        /** @var Country $country */
        $country = $this->getReference(CountryFixtures::COUNTRY_FIXTURE_REFERENCE);
        $issuer->setCountryId($country);

        $manager->persist($issuer);
        $manager->flush();

        $this->addReference(self::ISSUER_FIXTURE_REFERENCE, $issuer);
    }

    public function getDependencies(): array
    {
        return [
            CountryFixtures::class
        ];
    }
}
