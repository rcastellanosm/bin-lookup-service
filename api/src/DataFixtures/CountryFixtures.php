<?php

namespace App\DataFixtures;

use App\Entity\Country;
use Doctrine\Persistence\ObjectManager;

class CountryFixtures extends BaseFixture
{
    const COUNTRY_FIXTURE_REFERENCE = 'argentina-country';

    protected function loadData(ObjectManager $manager)
    {
       $country = (new Country())
            ->setName('Argentina')
            ->setCode('AR');

        $manager->persist($country);
        $manager->flush();

        $this->addReference(self::COUNTRY_FIXTURE_REFERENCE, $country);
    }
}
