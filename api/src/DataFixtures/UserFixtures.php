<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends BaseFixture
{
    private $roles = ['ROLE_USER', 'ROLE_ADMIN'];
    private $basePassword = 'equationlabs123';
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(User::class, 5, function (User $user) {
            $user->setEmail($this->faker->email);
            $user->setRoles([$this->faker->randomElement($this->roles)]);
            $user->setName($this->faker->firstName());
            $user->setLastname($this->faker->lastName);
            $user->setActive($this->faker->boolean());
            $user->setCreatedAt($this->faker->dateTime());

            $encodedPassword = $this->encoder->encodePassword($user, $this->basePassword);
            $user->setPassword($encodedPassword);
        });

        $manager->flush();
    }

}

