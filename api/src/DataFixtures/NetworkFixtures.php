<?php

namespace App\DataFixtures;

use App\Entity\Network;
use Doctrine\Persistence\ObjectManager;

class NetworkFixtures extends BaseFixture
{
    const NETWORK_FIXTURE_REFERENCE = 'network-reference';

    protected function loadData(ObjectManager $manager)
    {
        $network = new Network();

        $network->setName('Visa');
        $network->setType('Public');
        $network->setCreatedAt($this->faker->dateTime());

        $manager->persist($network);
        $manager->flush();

        $this->addReference(self::NETWORK_FIXTURE_REFERENCE, $network);

    }
}
