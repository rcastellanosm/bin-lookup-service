<?php

namespace App\DataFixtures;

use App\Entity\BinRange;
use App\Entity\Country;
use App\Entity\Issuer;
use App\Entity\Network;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Generator;

class BinRangeFixtures extends BaseFixture implements DependentFixtureInterface
{
    const A_BIN_RANGE_FROM = '459354';
    const A_BIN_RANGE_TO = '459355';

    /** @var Generator */
    protected $faker;

    public function loadData(ObjectManager $manager)
    {
        $binRange = new BinRange();

        /** @var Country $country */
        $country = $this->getReference(CountryFixtures::COUNTRY_FIXTURE_REFERENCE);

        /** @var Network $network */
        $network = $this->getReference(NetworkFixtures::NETWORK_FIXTURE_REFERENCE);

        /** @var Issuer $issuer */
        $issuer = $this->getReference(IssuerFixtures::ISSUER_FIXTURE_REFERENCE);

        $binRange
            ->setAccountRangeFrom(self::A_BIN_RANGE_FROM)
            ->setAccountRangeTo(self::A_BIN_RANGE_TO)
            ->setBrand('Black')
            ->setType('Credit')
            ->setPrepaid($this->faker->boolean())
            ->setCashback($this->faker->boolean())
            ->setCountry($country)
            ->setNetwork($network)
            ->setIssuer($issuer)
            ->setCreatedAt($this->faker->dateTime());

        $manager->persist($binRange);
        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            CountryFixtures::class,
            IssuerFixtures::class,
            NetworkFixtures::class,
        ];
    }
}
