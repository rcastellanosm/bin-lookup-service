<?php

namespace App\Subscriber;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserPasswordEncoderSubscriber implements EventSubscriberInterface
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            BeforeEntityPersistedEvent::class => 'userEncodeAndRemovePlainPassword',
            BeforeEntityUpdatedEvent::class  => 'userEncodeAndRemovePlainPassword'
        ];
    }

    /**
     * @param $event
     * @internal
     */
    public function userEncodeAndRemovePlainPassword($event)
    {
        $entity = $event->getEntityInstance();

        /** @var User $entity */
        if ($entity instanceof User && $entity->getPlainPassword()) {
            $entity->setPassword($this->passwordEncoder->encodePassword($entity, $entity->getPlainPassword()));
            $entity->eraseCredentials();
        }

    }
}
