<?php

namespace App\Subscriber;


use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ExceptionsNormalizerSubscriber implements EventSubscriberInterface
{
    private const JSON_API_HEADER = ['Content-Type' => 'application/vnd.api+json'];

    private $errorNormalizer;
    private $translator;

    public function __construct(NormalizerInterface $errorNormalizer, TranslatorInterface $translator)
    {
        $this->errorNormalizer = $errorNormalizer;
        $this->translator = $translator;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => [
                'normalize', EventPriorities::POST_RESPOND
            ],
        ];
    }

    public function normalize(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();

        $this->respond($exception, $event);
    }

    private function normalizeErrorResponse(\Throwable $exception): array
    {
        $error = $this->errorNormalizer->normalize($exception, null, []);

        $error['title'] = $this->translator->trans(get_class($exception));
        $error['detail'] = $this->translator->trans($exception->getMessage());

        unset($error['description']);
        unset($error['trace']);

        $errors['errors'][] = $error;

        return $errors;
    }

    private function respond($exception, ExceptionEvent $event): void
    {
        $event->setResponse(new JsonResponse(
                $this->normalizeErrorResponse($exception),
                400,
                self::JSON_API_HEADER)
        );
    }
}