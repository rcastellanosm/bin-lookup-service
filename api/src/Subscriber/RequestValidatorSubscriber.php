<?php

namespace App\Subscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Validators\RequestValidator;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class RequestValidatorSubscriber implements EventSubscriberInterface
{
    private $requestValidator;

    private $env;

    public function __construct(RequestValidator $requestValidator, ParameterBagInterface $env)
    {
        $this->requestValidator = $requestValidator;
        $this->env = $env;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => [
                'validate', EventPriorities::PRE_READ
            ],
        ];
    }

    public function validate(RequestEvent $event): void
    {
        if(preg_match( "/\/{$this->env->get('api_path_version')}(.*)?\/*/", $event->getRequest()->getPathInfo())) {
            $this->requestValidator->validate($event->getRequest());
        }
    }
}
