<?php

namespace App\Repository;

use App\Entity\BinRange;
use App\Entity\Country;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BinRange|null find($id, $lockMode = null, $lockVersion = null)
 * @method BinRange|null findOneBy(array $criteria, array $orderBy = null)
 * @method BinRange[]    findAll()
 * @method BinRange[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BinRangeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BinRange::class);
    }

    public function findBinRangeByCriteria(?array $parameters)
    {
        $qb = $this->createQueryBuilder('br')
            ->leftJoin(Country::class, 'c', Join::WITH, 'br.country = c.id')
            ->where(':binToFind >= br.accountRangeFrom')
            ->andWhere(':binToFind < br.accountRangeTo')
            ->andWhere('c.code = :countryCode')
            ->setParameter(':binToFind', $parameters['bin'])
            ->setParameter(':countryCode', $parameters['country']);

        return $qb->getQuery()->getOneOrNullResult();
    }
}
